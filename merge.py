# merge.py

import glob
from PyPDF2 import PdfFileWriter, PdfFileReader


def merger(output_path, input_paths):
    """function to merge pdf files
    
    Arguments:
        output_path {string} -- file name of output
        input_paths {list} -- list of paths
    """

    pdf_writer = PdfFileWriter()

    for path in input_paths:
        pdf_reader = PdfFileReader(path)
        for page in range(pdf_reader.getNumPages()):
            pdf_writer.addPage(pdf_reader.getPage(page))

    with open(output_path, "wb") as fh:
        pdf_writer.write(fh)


if __name__ == "__main__":
    paths = glob.glob("pdfile_0*.pdf")
    paths.sort()
    merger("merged_pdfile.pdf", paths)
